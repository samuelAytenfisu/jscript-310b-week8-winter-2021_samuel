
const body = document.querySelector('body');

let x = 0;
const animate = function() {
    x++;
    if (x <=255) {
        x++;
        body.style.backgroundColor = `rgb(${x},${x},${x})` 
        
        requestAnimationFrame(animate);
    } 
    
    
}
requestAnimationFrame(animate);

// const myPromise = new Promise(function(resolve, reject) {
//     setTimeout(function() {
//       if(Math.random() <= 0.5) {
//       reject();
//     } else {
//       resolve();
//     }
//     }, 1000);
//   });