


document.addEventListener('DOMContentLoaded', () => {
const body = document.querySelector('body');
let colorValue =  255;

const colorChangeInterval = setInterval(function (){
    if (colorValue > 0) {
        colorValue -= 5; 
        body.style.backgroundColor = `rgb(${colorValue},${colorValue},${colorValue})` 
    } else
    
    clearInterval(colorChangeInterval);
}, 400);
});
///body.style.backgroundColor =' rgb(500,500,500)';